#+title: Unit 9: Health Problems
#+author: Edward W.
#+EXPORT_FILE_NAME: index

:REVEAL_PROPERTIES:
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_THEME: serif
#+OPTIONS: toc:nil timestamp:nil num:nil
:end:
* Part 1 & 2
| */Supplementary/* | */Part One/* | */Part Two/* |
|-------------------+--------------+--------------|
| symptom           | tired        | matter       |
| temperature       | achy         | sympathy     |
|                   | rest         | cafeteria    |
|                   | chicken soup |              |
|                   | /hot tea/    |              |
|                   | honey        |              |
|                   | /hot shower/ |              |
** symptom (n.)
Your body's reaction to being sick.

[[file:pics/symptoms.jpg]]

** temperature (n.)
How hot or cold something is.

[[file:pics/thermometer.webp]]

** tired (adj.)
[[file:pics/tired.jpg]]
** achy (adj.)
[[file:pics/Achy-Back.jpg]]
** rest (v.)
[[file:pics/rest.webp]]
** chicken soup (n.)
Western people think chicken soup will help when you feel sick.

#+ATTR_HTML: :width 65%
[[file:pics/Chicken-Soup.webp]]
** honey (n.)
[[file:pics/honey.jpg]]
** matter (n.)
A topic to talk about.

"What's the matter?" = "What is wrong?"

** sympathy (n.)
Feeling sadness because someone else is sad.

[[file:pics/sympathy-empathy.webp]]
** cafeteria (n.)
A large room used for eating.

Usually found in schools or businesses.

[[file:pics/cafeteria.jpg]]
* Part 4
- co-workers
- /office/
- /boss/
- manage
- common
- advice
- employees
** co-workers (n.)
The people you work with in a business.

[[file:pics/coworkers.jpg]]
** manage (v.)
Controlling a team of people.

[[file:pics/Manager.webp]]
** common (adj.)
- Something that is easy to find.
- Not very special or different.
** advice (n.)
How someone should do something, or how someone should fix a problem.
- /problem:/ "I have a fever and a sore throat."
- /advice:/ "You should go home and get some rest."
** employees (n.)
A person that works at a company (not the boss or manager).
[[file:pics/employee-worker.jpg]]
* Part 5
| patients        | connects       | disease    |
| /India/         | /human beings/ | contagious |
| psychologist    | language       | /percent/  |
| /pretty strong/ | /joke/         | laughter   |
| mission         | studies        | club       |
| lead            | communication  |            |
| peace           | nonverbal      |            |
| /breathing/     | improve        |            |
** patient (n.)
A person seeing a doctor or staying in a hospital.
[[file:pics/doctor-patient.jpg]]
** psychologist (n.)
A doctor that studies the mind and behavior.
[[file:pics/psychologist.jpg]]
** mission (n.)
An important assignment or task you must do.
[[file:pics/soldiers.jpg]]
** lead (v)
To manage or control a group of people.

Deciding what other people need to do.

[[file:pics/lead.jpeg]]
** peace (n.)
To be calm; not fighting or in a war.
[[file:pics/peace.jpg]]
** connect (v.)
To bring things or people together.
[[file:pics/connect.jpg]]
** language (n.)
The words we use to talk or write to other people.

#+ATTR_HTML: :width 85%
[[file:pics/language.jpg]]
** studies (n.)
/science:/ studying something carefully to find the truth.

[[file:pics/science.webp]]
** communication (n.)
Different ways of sharing our feelings and ideas with other people.
#+ATTR_REVEAL: :frag (fade-left)
- talking
- writing
- e-mail
- phone calls
- hand movements
- facial expressions
** nonverbal (adj.)
How we share our feelings without using words or talking.

#+ATTR_HTML: :width 65%
[[file:pics/Non-Verbal-Communication.png]]
** improve (v.)
Make something better.
** disease (n.)
Something that is wrong with a person's body, making them sick.

[[file:pics/covid19.jpg]]
** contagious (adj.)
A disease that can easily move to another person.

[[file:pics/Sneeze.JPG]]
** laughter (n.)
[[file:pics/laugh.webp]]
** club (n.)
A group of people that do an activity together.

[[file:pics/photography-club.jpg]]

* Part 6
- diagram
- treat
- sprained
- ankle
- elevate
- brace
- /heart/

** diagram (n.)
A picture that helps you learn how something works.

#+ATTR_HTML: :width 55%
[[file:pics/diagram.png]]
** treat (v.)
Take care of a sickness or injury.

[[file:pics/treat.jpeg]]
** ankle (n.)
Part of the body that connects your legs and your feet.

[[file:pics/ankle.jpg]]
** sprained (adj.)
When you hurt your body by stretching or turning to far.

#+ATTR_HTML: :width 65%
[[file:pics/ankle-sprains.jpg]]
** elevate (v.)
Raise or lift something up to a higher place.

[[file:pics/elevator-inside.webp]]

American English: elevator

British English: lift
** brace (n.)
Something that will hold a body part and stop it from moving.

#+ATTR_HTML: :width 45%
[[file:pics/ankle-brace.jpg]]
